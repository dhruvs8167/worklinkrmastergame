﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class resetCursor : MonoBehaviour
{
    public CursorMode cursorMode = CursorMode.Auto;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.SetCursor(default, default, cursorMode);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
