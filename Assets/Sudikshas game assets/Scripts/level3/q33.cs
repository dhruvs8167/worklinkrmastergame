﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class q33 : MonoBehaviour
{
    string ans;
    public GameObject textFieldName;
    int ansStr, qAns, qint1, qint2, qint3,qint4,qint5;
    public Text scoreCard;
    public Text wrong;
    public Button qa1, qa2, qa3,qa4,qa5;
    void Start()
    {
        sceneManager.level = sceneManager.level + 1;
        qint1 = Random.Range(0, 4);
        qint2 = Random.Range(0, 4);
        qint3 = Random.Range(0, 4);
        qint4 = Random.Range(0, 4);
        qint5 = Random.Range(0, 4);
        qa1.image.sprite = keylevel3.sym[qint1];
        qa2.image.sprite = keylevel3.sym[qint2];
        qa3.image.sprite = keylevel3.sym[qint3];
        qa4.image.sprite = keylevel3.sym[qint4];
        qa5.image.sprite = keylevel3.sym[qint5];
        qAns = qint1 * 10000 + qint2 * 1000 + qint3 * 100 + qint4 * 10 + qint5 * 1;
        scoreCard.text = sceneManager.currentScore.ToString();
        wrong.text = sceneManager.wrongAnswer.ToString();
    }

    void Update()
    {
        sceneManager.timer = sceneManager.timer + Time.deltaTime;
        if (Input.GetKey("return"))
        {
            qq33();
        }
    }
    public void qq33()
    {
        ans = textFieldName.GetComponent<Text>().text;
        ansStr = int.Parse(ans);
        if (ansStr == qAns)
        {
            sceneManager.currentScore = sceneManager.currentScore + 1;
        }
        else sceneManager.wrongAnswer = sceneManager.wrongAnswer + 1;
        if (sceneManager.wrongAnswer == 3)
        {
            SceneManager.LoadScene(21);
        }
        else
            SceneManager.LoadScene(21);
    }
}

