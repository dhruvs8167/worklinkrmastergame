﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class keylevel1 : MonoBehaviour
{
    public Animator clock;
    int i=0,k=0;
    public GameObject heading1;

    public Button[] B1;
    public Button[] B2;


    public Sprite[] numbers;
    public Sprite[] symbols;

    public int[] randno = new int[30];
    public int[] randsym = new int[30];

 
    static public Sprite[] sym= new Sprite[30];
    static public Sprite[] num = new Sprite[30];
    int j;



    void Start()
    {
        sceneManager.stage = sceneManager.stage + 1;
        for (j=0;j<3;j++)
        {
         randsym[j] = Random.Range(k,k+4);
         B1[j].image.sprite = numbers[j];
           
                B2[j].image.sprite = symbols[randsym[j]];
                sym[j] = symbols[randsym[j]];
            
            k = k + 4;
        }
        for(j=0;j<20;j++)
        {
            num[j] = numbers[j];
 
        }
      

    }
    private void Update()
    {
        i++;
        if (i > 700)
            clock.SetBool("isStable", false);
        if (i > 900)
            SceneManager.LoadScene(Random.Range(3,5));
    }
}
