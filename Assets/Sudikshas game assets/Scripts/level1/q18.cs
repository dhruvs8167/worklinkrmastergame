﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class q18 : MonoBehaviour
{
    string ans;
    public GameObject textFieldName;
    public Text scoreCard;
    int ansStr, qAns, qint1, qint2,qint3;
    public Text wrong;
    public Button qa1, qa2,qa3;
    void Start()
    {
        sceneManager.level = sceneManager.level + 1;
        qint1 = Random.Range(0, 3);
        qint2 = Random.Range(0, 3);
        qint3 = Random.Range(0, 3);
        qa1.image.sprite = keylevel1.sym[qint1];
        qa2.image.sprite = keylevel1.sym[qint2];
        qa3.image.sprite = keylevel1.sym[qint3];
        qAns = qint1 + qint2-qint3;
        scoreCard.text = sceneManager.currentScore.ToString();
        wrong.text = sceneManager.wrongAnswer.ToString();
    }

    void Update()
    {
        sceneManager.timer = sceneManager.timer + Time.deltaTime;
        if (Input.GetKey("return"))
        {
            qq18();
        }
    }
    public void qq18()
    {
        ans = textFieldName.GetComponent<Text>().text;
        ansStr = int.Parse(ans);
        if (ansStr == qAns)
        {
            sceneManager.currentScore = sceneManager.currentScore + 1;
        }
        else sceneManager.wrongAnswer = sceneManager.wrongAnswer + 1;
        Debug.Log("score=" + sceneManager.currentScore + "wrong ans=" + sceneManager.wrongAnswer + "time=" + sceneManager.timer);
        if (sceneManager.wrongAnswer == 3)
        {
            SceneManager.LoadScene(21);
        }
        else
        SceneManager.LoadScene(11);
    }
}
