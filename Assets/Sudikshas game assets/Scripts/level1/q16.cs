﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class q16 : MonoBehaviour
{
    string ans;
    public GameObject textFieldName;
    int ansStr, qAns, qint1, qint2;
    public Text scoreCard;
    public Text wrong;
    public Button qa1, qa2;
    void Start()
    {
            sceneManager.level = sceneManager.level + 1;
            qint1 = Random.Range(0, 3);
            qint2 = Random.Range(0, 3);
            qa1.image.sprite = keylevel1.sym[qint1];
            qa2.image.sprite = keylevel1.sym[qint2];
            qAns = qint1 - qint2;
            scoreCard.text = sceneManager.currentScore.ToString();
        wrong.text = sceneManager.wrongAnswer.ToString();
    }

    void Update()
    {
        sceneManager.timer = sceneManager.timer + Time.deltaTime;
        if (Input.GetKey("return"))
        {
            qq16();
        }
    }
    public void qq16()
    {
        ans = textFieldName.GetComponent<Text>().text;
        ansStr = int.Parse(ans);
        if (ansStr == qAns)
        {
            sceneManager.currentScore = sceneManager.currentScore + 1;
        }
        else sceneManager.wrongAnswer = sceneManager.wrongAnswer + 1;
        Debug.Log("score=" + sceneManager.currentScore + "wrong ans=" + sceneManager.wrongAnswer + "time=" + sceneManager.timer);
        if (sceneManager.wrongAnswer == 3)
        {
            SceneManager.LoadScene(21);
        }
       else
        SceneManager.LoadScene(Random.Range(9, 11));
    }
}

