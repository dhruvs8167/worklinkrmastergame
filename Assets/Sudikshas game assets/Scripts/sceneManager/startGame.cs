﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class startGame : MonoBehaviour
{
    public void startGameStage1()
    {
        sceneManager.timer = 0;
        sceneManager.wrongAnswer = 0;
        sceneManager.stage = 0;
        sceneManager.level = 0;
        sceneManager.currentScore = 0;
        SceneManager.LoadScene("keylevel1");
    }
}