﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
public class gameOver : MonoBehaviour
{
    public Text percentage, timeTaken;
    public Text playerName;
     void Start()
    {
        percentage.text = sceneManager.currentScore.ToString();
        timeTaken.text = sceneManager.timer.ToString();
        playerName.text = sceneManager.playerName.ToString();
        if(sceneManager.currentScore>PlayerPrefs.GetInt("highScore"))
        {
            PlayerPrefs.SetInt("highscore", (sceneManager.currentScore));
            PlayerPrefs.SetFloat("time", (sceneManager.timer));
            PlayerPrefs.SetString("name", (sceneManager.playerName));
        }
    }
    public void restart()
    {
        SceneManager.LoadScene(0);

    }
    public void LoadSceneByNameCall(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
        //StartCoroutine(LoadSceneByName(sceneName));
    }
    public void exitGame()
    {
        Application.Quit();
    }
}
