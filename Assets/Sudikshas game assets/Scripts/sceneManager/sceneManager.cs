﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class sceneManager : MonoBehaviour
{
    public GameObject startButton, startButtonName,textFieldName,inputField;
    public static string playerName;
    public static int currentScore;
    public static int stage;
    public static int level;
    public static int wrongAnswer;
    public static float timer;
    public TMP_Text highScore;
    public TMP_Text playerNameText;
    public TMP_Text timeTaken;

    void Start()
    {
        highScore.text = PlayerPrefs.GetInt("highscore").ToString();
        playerNameText.text = PlayerPrefs.GetString("name").ToString();
        timeTaken.text = PlayerPrefs.GetFloat("time").ToString();
        startButtonName.SetActive(true);
        inputField.SetActive(false);
        startButton.SetActive(false);
    }
    public void enterName()
    {
        startButtonName.SetActive(false);
        inputField.SetActive(true);
        startButton.SetActive(true);

    }
    public void startGame()
    {
        playerName = textFieldName.GetComponent<Text>().text;
        SceneManager.LoadScene("introStart");
    }
    
    public void exitGame()
    {
        Application.Quit();
    }
    











}
