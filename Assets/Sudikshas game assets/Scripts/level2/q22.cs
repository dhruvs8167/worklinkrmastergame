﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class q22 : MonoBehaviour
{
    string ans;
    public GameObject textFieldName;
    public Text scoreCard;
    public Text wrong;
    int ansStr, qAns, qint1, qint2, qint3;
    public Button qa1, qa2, qa3;
    void Start()
    {
        sceneManager.level = sceneManager.level + 1;
        qint1 = Random.Range(0, 4);
        qint2 = Random.Range(0, 4);
        qint3 = Random.Range(0, 4);
        qa1.image.sprite = keylevel2.sym[qint1];
        qa2.image.sprite = keylevel2.sym[qint2];
        qa3.image.sprite = keylevel2.sym[qint3];
        qAns = qint1 + qint2 * qint3;
        scoreCard.text = sceneManager.currentScore.ToString();
        wrong.text = sceneManager.wrongAnswer.ToString();
    }

    void Update()
    {
        sceneManager.timer = sceneManager.timer + Time.deltaTime;
        if (Input.GetKey("return"))
        {
            qq22();
        }
    }
    public void qq22()
    {
        ans = textFieldName.GetComponent<Text>().text;
        ansStr = int.Parse(ans);
        if (ansStr == qAns)
        {
            sceneManager.currentScore = sceneManager.currentScore + 1;
        }
        else sceneManager.wrongAnswer = sceneManager.wrongAnswer + 1;
        Debug.Log("score=" + sceneManager.currentScore + "wrong ans=" + sceneManager.wrongAnswer + sceneManager.wrongAnswer + "ans=" + qAns);
        if (sceneManager.wrongAnswer ==3)
        {
            SceneManager.LoadScene(21);
        }
            else
        SceneManager.LoadScene(15);
    }
}
