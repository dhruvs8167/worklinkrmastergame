﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class scoreCard : MonoBehaviour
{
    public Text score, time, wrongAns;
    void Start()
    {
        score.text = sceneManager.currentScore.ToString();
        time.text = sceneManager.timer.ToString();
        wrongAns.text = sceneManager.wrongAnswer.ToString();
        
    }
    void Update()
    {
        if (Input.GetKey("return"))
        {
            nextScene();
        }
    }
    public void nextScene()
    {
        SceneManager.LoadScene("keylevel2");
    }
}
