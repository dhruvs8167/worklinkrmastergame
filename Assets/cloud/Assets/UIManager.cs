﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace cloud
{
    // this script takes input and aoutpu from ui
    public class UIManager : MonoBehaviour
    {
        public int breakPoint;
        public GameObject Finish;
        // Start is called before the first frame update
        public float waitTime = 2;
        public InputField CombinedAnswer;
        public string AnswerAlphabet;
        public string AnswerNumeric;
        public Text Score;
        public Text CodeDisplay;
        public string FullCode;
        public GameObject GameFunctions;
        public int markstotal, numeric_Marks, alphabet_marks, marksForThisLevel = 0;
        public int level = 0;
        public int[] levelDificulty;// store dificulty of all levels
        public Animator Cloud1;
        public GameObject[] wind = new GameObject[2];
        public Animator Cloud2;
        bool cloudSelector = true;
        public Text LevelText;
        public GameObject SubmitButtonRef;
        public GameObject InputPnnel;
        //public GameObject NextPannel;
        public Button RetryButton;
        public int trial = 1;
        public GameObject Rain;
        public GameObject Rainbow;
        public GameObject StartButton;
        public Animator RetryAnim, Trial1;
        void Start()
        {
            markstotal = numeric_Marks = alphabet_marks = 0;

            StartButton.SetActive(true);
            Score.text = " ";
            InputPnnel.SetActive(false);
            // NextPannel.SetActive(false);
            CloudBlowStop();
            Finish.SetActive(false);
        }


        public void startButton()
        {
            trial = 0;
            StartCoroutine(DisplayString(levelDificulty[level], levelDificulty[level], false));
            StartButton.SetActive(false);
        }


        IEnumerator DisplayString(int no, int alpha, bool retry)// will genrate random no if retry is false
        {  // hide submit button
            Score.text = " ";
            InputPnnel.SetActive(false);


            LevelText.text = "Level " + (level + 1).ToString();
            // genrating a new code  
            if (!retry)
            {
                GameFunctions.GetComponent<showTheString>().GenrateA_RandomNumber(no);
                GameFunctions.GetComponent<showTheString>().GenrateA_RandomString(alpha);
            }
            // storing numreic and alphabet sting in one string for display;
            FullCode = GameFunctions.GetComponent<showTheString>().stored_String_Alphabet + GameFunctions.GetComponent<showTheString>().stored_String_Numeric;

            for (int i = 0; i < FullCode.Length; i++)
            {
                yield return new WaitForSeconds(0.01f);
                CodeDisplay.text = FullCode[i].ToString();
                CloudBlow();
                yield return new WaitForSeconds(2f);
                CodeDisplay.text = "";
                yield return new WaitForSeconds(.5f);
                CloudBlowStop();

            }
            //Display submit button
            InputPnnel.SetActive(true);
            yield return null;
        }


        private void CloudBlow()
        {
            if (cloudSelector == true)
            {
                Cloud1.SetBool("blow", true);
                wind[0].SetActive(true);
                cloudSelector = false;
            }
            else
            {
                Cloud2.SetBool("blow", true);
                wind[1].SetActive(true);
                cloudSelector = true;

            }
        }


        private void CloudBlowStop()
        {
            Cloud1.SetBool("blow", false);
            Cloud2.SetBool("blow", false);
            wind[0].SetActive(false);
            wind[1].SetActive(false);
        }


        public void submitButton()
        {
            Debug.Log("answer submited");
            marksForThisLevel = 0;
            SplitStrings();
            string a = GameFunctions.GetComponent<showTheString>().Sorted_String_Numeric;// get sorted string of numbers
            numeric_Marks = GameFunctions.GetComponent<showTheString>().match_Ansewr(a, AnswerNumeric);// compare marks for numerics
            marksForThisLevel = a.Length;
            a = GameFunctions.GetComponent<showTheString>().Sorted_String_Alphabet;
            marksForThisLevel = marksForThisLevel + a.Length;
            alphabet_marks = GameFunctions.GetComponent<showTheString>().match_Ansewr(a, AnswerAlphabet);
            markstotal = numeric_Marks + alphabet_marks;
            resetvaluesFunc();
            // enableRetryOrNextPannel(true);
            //level++
            Score.text = "Score for this level " + markstotal.ToString() + "/" + marksForThisLevel.ToString();
            if (markstotal < marksForThisLevel && trial == 0)
            {
                Retry();
                cryCloud(true);
                trial = 1;
            }
            else if (markstotal == marksForThisLevel)
            {

                Happy(true);
                NextLevel();
                trial = 0;
            }
            else
            {
                // fail
                Finish.SetActive(true);
            }




            //  StartCoroutine(DisplayString(levelDificulty[level], levelDificulty[level], false));
            //Hide submit button;
            InputPnnel.SetActive(false);
            CombinedAnswer.text = "";// seset input field


        }

        

        public void NextLevel()// load next level activate by button
        {
            trial = 0;
            StartCoroutine(NExtLevelCoroutine());
        }

        IEnumerator NExtLevelCoroutine()
        {
            yield return new WaitForSeconds(waitTime);
            Happy(false);
            cryCloud(false);
            // trial = 1;
            level++;

            if (level > levelDificulty.Length - 1)/// level grater tha ntotal lwngth o level
            {
                Debug.Log("Finish");
                Finish.SetActive(true);
                //Call Retry
                //Retry();
            }


            else
            {
                StartCoroutine(DisplayString(levelDificulty[level], levelDificulty[level], false));
                //enableRetryOrNextPannel(false);
                //next level
            }

            yield return null;
        }



        public void Retry()// retry
        {
            StartCoroutine(RetryCoroutine());
        }
        public IEnumerator RetryCoroutine()
        {
            RetryAnim.SetTrigger("play");
            //PLay Nimation or retry
            yield return new WaitForSeconds(waitTime);
            StartCoroutine(DisplayString(levelDificulty[level], levelDificulty[level], true));
            //enableRetryOrNextPannel(false);
            cryCloud(false);
            //trial++;

            yield return null;
        }



        void resetvaluesFunc()// reset values of inserted answer
        {
            Debug.Log("reset String");
            AnswerAlphabet = null;
            AnswerNumeric = null;

        }

        void cryCloud(bool cry)
        {
            Cloud1.SetBool("cry", cry);
            Rain.SetActive(cry);
            Cloud2.SetBool("cry", cry);
        }
        void Happy(bool happy)
        {
            cryCloud(false);
            Rainbow.SetActive(happy);

        }

        public void NextGame()
        {
             GameObject.FindGameObjectWithTag("GameController").GetComponent<LoadSceneMAster>();
        }
      
        public void SplitStrings()
        {
            resetvaluesFunc();
            breakPoint = 0;
            string temp = CombinedAnswer.text.ToUpper();
            for (int i = 0; i < CombinedAnswer.text.Length; i++)
            {
                if (CombinedAnswer.text[i] >= '0' && CombinedAnswer.text[i] <= '9')
                {
                    Debug.Log("finding break point");
                    breakPoint = i;
                    break;
                }
            }

            for (int i = 0; i < breakPoint; i++)
            {
                AnswerAlphabet = AnswerAlphabet + CombinedAnswer.text[i].ToString();
            }
            for (int i = breakPoint; i < CombinedAnswer.text.Length; i++)
            {
                AnswerNumeric = AnswerNumeric + CombinedAnswer.text[i].ToString();
                print(AnswerNumeric);
            }


        }

        public void loadNextGame(int SceneLoad)
        {
            GameObject.FindGameObjectWithTag("GameController").GetComponent<LoadSceneMAster>().LoadSceneCall(SceneLoad);
        }
        public void loadNextGameByName(string SceneLoad)// by name
        {
            GameObject.FindGameObjectWithTag("GameController").GetComponent<LoadSceneMAster>().LoadSceneCall(SceneLoad);
        }
    }
}