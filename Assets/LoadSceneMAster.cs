﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//namespace Master{
public class LoadSceneMAster : MonoBehaviour    {
        public Text percentageText;
        public Image loadingImage;
        public GameObject LoadingBarsParent;
        public int CurrentScene;
        // Start is called before the first frame update
        void Start()
        {
        loadingScreenContentactive(false);
        }

        void loadingScreenContentactive(bool a)
        {
            percentageText.enabled = a;
            LoadingBarsParent.SetActive(a);
            loadingImage.enabled = a;
        }
        private void Awake()
        {
            DontDestroyOnLoad(this.gameObject);
        }
        // Update is called once per frame
        void Update()
        {

        }
    public IEnumerator LoadScene(int sceneIndex)
    {
        loadingScreenContentactive(true);
        yield return null;
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        LoadingBarsParent.SetActive(true);
        while (operation.isDone == false)
        {
            //float percentage;
            //whilecount++;
            //percentage = operation.progress / 0.9f;
            float progress = Mathf.Clamp01(operation.progress / .9f);
            percentageText.text = "Loading " + (100 * progress).ToString() + '%';
            //percent1.text = whilecount.ToString();
            //Debug.Log (progress);
            loadingImage.fillAmount = progress;
            yield return null;

        }
        loadingScreenContentactive(false);
    }
    public IEnumerator LoadScene(string sceneIndex)
    {
        loadingScreenContentactive(true);
        yield return null;
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        LoadingBarsParent.SetActive(true);
        while (operation.isDone == false)
        {
            //float percentage;
            //whilecount++;
            //percentage = operation.progress / 0.9f;
            float progress = Mathf.Clamp01(operation.progress / .9f);
            percentageText.text = "Loading " + (100 * progress).ToString() + '%';
            //percent1.text = whilecount.ToString();
            //Debug.Log (progress);
            loadingImage.fillAmount = progress;
            yield return null;

        }
        loadingScreenContentactive(false);
    }

    // overloaded fucntions
    public void LoadSceneCall(int a)
        {
            StartCoroutine(LoadScene(a));
        }
    public void LoadSceneCall(string sceneName)
    {
        StartCoroutine(LoadScene(sceneName));
    }


    public void exitGame()
        {
            Application.Quit();
        }

        public void NextScene()
        {
            CurrentScene = SceneManager.GetActiveScene().buildIndex;
        int loadScene= CurrentScene+1;
        Debug.Log("Loading next");
        StartCoroutine( LoadScene(loadScene)) ;
        }
    }
//}