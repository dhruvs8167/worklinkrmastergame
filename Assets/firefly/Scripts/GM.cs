﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GM : MonoBehaviour
{
    public int dis = 0;
    public GameObject ScorePanal1;
    
    public Text t;
    public Text ScoreTectUI;
    public GameObject ScorePanal;
    [HideInInspector]
    public int Score=0;
    [HideInInspector]
    public int totaltapcount=0;
    int i = 0;
    public GameObject[] bottels;
    int bottalcount;
    int scor;
    // Start is called before the first frame update
    void Start()
    {
        Score = 0;
        i = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //  PlayerPrefs.SetInt("Score", sc);
        //if(totaltapcount == 14)
        //{
        //    ScorePanal1.SetActive(true);
        //}
        ScoreTectUI.text = "Score: " + Score;
        t.text = "Score: " + Score;
        
    }
    public void ActiBottel()
    {
        //if(totaltapcount == 2)
        //{
        //    bottalcount++;
        //}
        if (i == 9)
        {
            Done();
        }
        Debug.Log(i);
        bottels[i].SetActive(false);
        StartCoroutine(Newbottel());

    }

    IEnumerator Newbottel()
    {
        yield return new WaitForSeconds(1.5f);
        if (i != 9)
        {
            bottels[i + 1].SetActive(true);
        }
        i++;
    }
    public void Done()
    {
       
        ScorePanal.SetActive(false);
        ScorePanal1.SetActive(true);
        
    }
    public void Playagain()
    {
        SceneManager.LoadScene("One");
    }
    public void Exit()
    {
        Application.Quit();
    }
}
