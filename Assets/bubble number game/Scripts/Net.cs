﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Net : MonoBehaviour
{
    public Texture2D mouse1;
    public Texture2D net;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
         // initializing the ray
    
    public GameObject Catch;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 spawnPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            spawnPosition.z = 0.0f;
            GameObject objectInstance = Instantiate(Catch, spawnPosition, Quaternion.Euler(new Vector3(0, 0, 0)));
            setMouse();
        }
    
        if (Input.GetMouseButtonUp(0))
            {
                setnet();
            }
        
    }

    public void setMouse()
    {
        Cursor.SetCursor(mouse1, hotSpot, cursorMode);
    }

    public void setnet()
    {
        Cursor.SetCursor(net, hotSpot, cursorMode);

    }
  
}
