﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
public class ChangeSceen : MonoBehaviour
{
    public Text scoree;
    int sc;
    float totaltime;
    public Animator ar;
    public GameObject black,black2;
    // Start is called before the first frame update
    void CreateText()
    {
        //Path of the file
        string path = Application.dataPath + "/Score.txt";
        //Create File if it doesn't exist
        if (!File.Exists(path))
        {
            File.WriteAllText(path, "Score log \n\n");
        }
        //Content of the file
        string content = "Score: " + sc + "\n";
        string time = "Total Time " + totaltime + "\n";
        //Add some to text to it
        File.AppendAllText(path, content);
        File.AppendAllText(path, time);
    }
    void Start()
    {
        
        sc = PlayerPrefs.GetInt("Score", 0);
        sc = sc - 1;
        scoree.text = "Score: " + sc;
        StartCoroutine("Fade");
        float t1, t2, t3, t4, t5;
        t1 = PlayerPrefs.GetFloat("time1", 0);
        t2 = PlayerPrefs.GetFloat("time2", 0);
        t3 = PlayerPrefs.GetFloat("time3", 0);
        t4 = PlayerPrefs.GetFloat("time4", 0);
        t5 = PlayerPrefs.GetFloat("time5", 0);
        totaltime = t1 + t2 + t3 + t4 + t5;
        Debug.Log(totaltime);
        CreateText();
    }
    IEnumerator Fade()
    {
        yield return new WaitForSeconds(0.8f);
        black2.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void Begin()
    {
        PlayerPrefs.SetInt("Score", 0);
        black.SetActive(true);
        // ar.Play("first");
        StartCoroutine("FadeI");
    }
    public void Exit()
    {
        Application.Quit();
    }
    
    IEnumerator FadeI()
    {
        

            yield return new WaitForSeconds(0.8f);
            SceneManager.LoadScene("Play1");

        
    }



}
