﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class Force : MonoBehaviour, IPointerDownHandler
{
  


    int sc = 0;
    public int speed = 4;
    private void Start()
    {
        this.gameObject.layer = 0;
        // this.GetComponent<Rigidbody2D>().simulated = false;
        Vector2 impulse;
        impulse = new Vector2(1, 1);

        this.GetComponent<Rigidbody2D>().AddForce(impulse, ForceMode2D.Impulse);
        g = FindObjectOfType(typeof(GameManager1)) as GameManager1;
    }

    private void Update()
    {
        Vector3 a;
        a = this.GetComponent<Rigidbody2D>().velocity;
        a.Normalize();
        this.GetComponent<Rigidbody2D>().velocity = a * speed;


    }
    public GameManager1 g;
    public Vector2 impulse;
    public Vector2 left = new Vector2(-5.0f, 0.0f);
    public Vector2 right = new Vector2(5.0f, 0.0f);
    public Vector2 down = new Vector2(0.0f, 5.0f);
    public Vector2 up = new Vector2(0.0f, -5.0f);

    //int[] a = new int[20];
    bool m_oneTime = true;
    // int i = 0;

    // Use this for initialization

    /*
    void FixedUpdate()
    {
        impulse = new Vector2(3f, 3f);
        if (m_oneTime== true)
        {
            GetComponent<Rigidbody2D>().AddForce(impulse, ForceMode2D.Impulse);
            m_oneTime = false;
        }
    }
    */
    // Update is called once per frame

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "1")
        {
            GetComponent<Rigidbody2D>().AddForce(left);
            //Destroy(gameObject);
        }
        if (col.gameObject.tag == "2")
        {
            GetComponent<Rigidbody2D>().AddForce(right);
        }
        if (col.gameObject.tag == "3")
        {
            GetComponent<Rigidbody2D>().AddForce(down);
        }
        if (col.gameObject.tag == "4")
        {
            GetComponent<Rigidbody2D>().AddForce(up);
        }

      //  if (col.gameObject.tag == "catch")
        //{
          //  string c = gameObject.name;
            //int x = 0;

            //int.TryParse(c, out x);
            // Debug.Log(x);
            // if (g != null)
            // {
            //g.s = x;
          //g.Score();
            // }
            //Destroy(gameObject);
            //Destroy(col.gameObject);

//        }

    }
    /*public void Onballclick()
       {
          string c = gameObject.name;
          int x = 0;

          int.TryParse(c, out x);
          sc =  PlayerPrefs.GetInt("Score", 0);
          sc = sc + 1;
          PlayerPrefs.SetInt("Score", sc);
          // Debug.Log(x);
          // if (g != null)
          // {
          g.s = x;
          g.Score();
          // }
          Destroy(gameObject);
       }
       */
    public void OnPointerDown(PointerEventData pointerEventData)
    {
        //Output the name of the GameObject that is being clicked
        string c = gameObject.name;
        int x = 0;

        int.TryParse(c, out x);
        sc = PlayerPrefs.GetInt("Score", 0);
        sc = sc + 1;
        PlayerPrefs.SetInt("Score", sc);
        // Debug.Log(x);
        // if (g != null)
        // {
        g.s = x;
        g.Score();
        // }
        Destroy(gameObject);
    }
}