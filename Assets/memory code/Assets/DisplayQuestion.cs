﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayQuestion : MonoBehaviour
{
    public GameObject ScoreSaversForGraph;
    public int score=0;
    public Text ScoreText;
    public GenrateAQuestion QuestionObject;
    public RawImage ScreenQuad;
    public GameObject ImageHolder;
    public Text CodeDisplay;
    public int[] ImageNumbers;
    public int[] codeNumbers;
    public GameObject
        CharacterHolder;
    public string answerString;

    public RawImage[] DisplayGenratedImages;
    public GameObject AnswerInserted;
    public int[] askedQuestion;
    public GameObject SubmitButon;
    public GameObject StartAndEndObj;
    int currentQuestionNo=1;
    // private int 
    void Start()
    {
        EnableLargeDisplay();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator DisplayImages()
    {
        EnableLargeDisplay();

        ImageNumbers = QuestionObject.GetComponent<GenrateAQuestion>().ImageNumbers;
        codeNumbers  = QuestionObject.GetComponent<GenrateAQuestion>().codeNumbers;

        yield return new WaitForSeconds(0.1f);
        for(int i = 0; i < ImageNumbers.Length;i++)
        {
            ScreenQuad.texture = ImageHolder.GetComponent<InitializeImages>().textList [ QuestionObject.GetComponent<GenrateAQuestion>().ImageNumbers[i] ];

            CodeDisplay.text = CharacterHolder.GetComponent<AllCharacters>().Obj[codeNumbers[i]].CharacterCodeString;
            yield return new WaitForSeconds(3f);
        }
        yield return null;

        DisableLargeDisplay();
        WhatIsCodeForThis();
        DisplayImageCode();

       

    }



    public void WhatIsCodeForThis()
    {
        answerString = "";
        askedQuestion = new int[ImageNumbers.Length];
        for (int i = 0; i < ImageNumbers.Length; i++)
        {
            askedQuestion[i] = Random.Range(0, ImageNumbers.Length);
            answerString = answerString+ CharacterHolder.GetComponent<AllCharacters>().Obj[codeNumbers[askedQuestion[i]]].CharacterCodeString;
        }
        
    }

    public void displayfunction()
    {
        StartCoroutine(DisplayImages());
    }

    public void DisplayImageCode()//Display 4 images on aw image
    {

        for (int i = 0; i < ImageNumbers.Length; i++)
        {
            DisplayGenratedImages[i].texture = ImageHolder.GetComponent<InitializeImages>().textList[QuestionObject.GetComponent<GenrateAQuestion>().ImageNumbers[askedQuestion[i]]];
        }
        AnswerInserted.SetActive(true);
        AnswerInserted.GetComponent<InputField>().text = "";
    }

    public void EnableLargeDisplay()// enable only large RAW image
    {
        AnswerInserted.SetActive(false);
        //AnswerInserted.enabled = false;
        ScreenQuad.enabled = true;
        CodeDisplay.enabled = true;
        SubmitButon.SetActive(false);
        for (int i = 0; i < 4; i++)
        {
            DisplayGenratedImages[i].enabled = false;
        }
    }
    public void DisableLargeDisplay()
    {
        AnswerInserted.SetActive(true);
        ScreenQuad.enabled = false;
        CodeDisplay.enabled = false;
        for (int i = 0; i < 4; i++)
        {
            DisplayGenratedImages[i].enabled = true;
        }
        SubmitButon.SetActive(true);
    }
    public void checklength()
    {
        if (AnswerInserted.GetComponent<InputField>().text.Length == 4)
        {
            SubmitButon.SetActive(true);
        }
        else
            SubmitButon.SetActive(false);

    }
    public int CheckAnswer()
    {
        string enteredAnswer = AnswerInserted.GetComponent<InputField>().text;
        answerString = answerString.ToLower();
        int ScoreForThisAnswer = 0;
       
        enteredAnswer = enteredAnswer.ToLower();
        for (int i = 0; i < enteredAnswer.Length; i++)
        {

            if (enteredAnswer[i] == answerString[i])
            {
                ScoreForThisAnswer++;
            }
          /*  if (ScoreForThisAnswer > 3)
            {
                //ScoreForThisAnswer = ScoreForThisAnswer + 4;
                ScoreForThisAnswer = 4;

            }*/



        }

        if (AnswerInserted.GetComponent<InputField>().text.ToLower() == answerString.ToLower())
        {
            Debug.Log("Correnct Answer");

        }
       /* if (AnswerInserted.GetComponent<InputField>().text.Length == 4)
        {
            SubmitButon.SetActive(true);
        }
        else
            SubmitButon.SetActive(false);*/

        return ScoreForThisAnswer;
    }

    public void SubmitAnsewr()
    {
        
        print("Ansewr Submited");
        int currentQuestionAnswer = CheckAnswer();
        StartAndEndObj.GetComponent<StartAndEndScript>().playAnimation(currentQuestionAnswer);
        ScoreSaversForGraph.GetComponent<ScoreAnalysisScript>().RecordScore(currentQuestionAnswer);
        score = score +currentQuestionAnswer;
        ScoreText.text = "Score " + score.ToString()+ "/"+(8*currentQuestionNo).ToString();
        currentQuestionNo++;
        
        StartAndEndObj.GetComponent<StartAndEndScript>().NextQuestion();

    }
}
