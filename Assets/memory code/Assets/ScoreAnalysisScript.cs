﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreAnalysisScript : MonoBehaviour
{   
    
    public int[] Score = new int[5];
    public int QuestionNo=0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
    public void RecordScore(int score)
    {
        Score[QuestionNo] = score;

        QuestionNo++;

    }
}
