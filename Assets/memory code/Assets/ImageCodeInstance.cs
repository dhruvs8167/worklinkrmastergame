﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]

public class ImageCodeInstance
{
    // Start is called before the first frame update
    //public Image ImageForCode;
    public char MyCode;
    //public Texture2D TextureOfImage;
    public bool ImageAvailable= true;    

}
