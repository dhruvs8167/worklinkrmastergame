﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenrateAQuestion : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject characterHolder;
    public GameObject ImageHolder;

    
    public int[] ImageNumbers;
    public int[] codeNumbers;

    public int dificulty=4;
    void Start()
    {
        
    }



    public void GenrateImageAndCode(int Dificulty)
    {
        ImageHolder.GetComponent<InitializeImages>().ResteTheAssigned();// Reset bool values 
        characterHolder.GetComponent<AllCharacters>().ResteTheAssigned();
        ImageNumbers = new int[Dificulty];
        codeNumbers = new int[Dificulty];

        for (int i = 0; i < Dificulty; i++)
        {
            GenrateOnceMore:
            int a = Random.Range(0, ImageHolder.GetComponent<InitializeImages>().textList.Length - 1);
            if (ImageHolder.GetComponent<InitializeImages>().availability[a] == true)
            {
                ImageNumbers[i] = a;
                ImageHolder.GetComponent<InitializeImages>().availability[a] = false;
            }
            else
                goto GenrateOnceMore;


            GenrateCharCode:
            a = Random.Range(0, characterHolder.GetComponent<AllCharacters>().Obj.Length);
            if (characterHolder.GetComponent<AllCharacters>().Obj[a].avaliability == true)
            {
                codeNumbers[i] = a;
                characterHolder.GetComponent<AllCharacters>().Obj[a].avaliability = true;
            }
            else
                goto GenrateCharCode;

       }

        


    }




}
