﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class marksReadScript : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject DisplayBar;
    public Image MarksBarFill; 
    public int BarNo;
    GameObject MarksContainer;
    public Text QuestionNoText;
    public Text Marks_Text_reference;
    void Start()
    {
        MarksContainer = GameObject.FindGameObjectWithTag("Score");
        QuestionNoText.text = 'Q'+BarNo.ToString();
        StartCoroutine(FillBars());
    }

    // Update is called once per frame
    IEnumerator FillBars()
    {
        yield return new WaitForSeconds(0.5f);
        //for (int i = 0; i < 10; i++)
        {
            int no = MarksContainer.GetComponent<ScoreAnalysisScript>().Score[BarNo-1];
            Marks_Text_reference.text =no.ToString()+" Marks";
            for (float a =0;a<=no;a=a+0.5f)
            {
                MarksBarFill.fillAmount = a / 4;
                yield return new WaitForSeconds(0.1f);
            }

            
            


        }
        yield return null;
    }
    void Update()
    {
        
    }
}
