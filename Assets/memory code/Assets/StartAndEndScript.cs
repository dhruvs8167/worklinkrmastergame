﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAndEndScript : MonoBehaviour
{
    public GameObject GetResults;
    public GameObject Loadingscreen;
    public int NoOfLevels = 4;
    public int currentLevel = 0;
    public GameObject DisplayQuestionObject;
    public GameObject CodeGenObject;
    public GameObject Corect, partialCorrect ,incorrect;
    // Start is called before the first frame update
    void Start()
    {
        Corect.SetActive(false);
        partialCorrect.SetActive(false);
        incorrect.SetActive(false);
        NextQuestion();
        GetResults.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    IEnumerator StartGame()
    {
        Loadingscreen.SetActive(true);
        yield return new WaitForSeconds(2f);
        Loadingscreen.SetActive(false);
        yield return null;
        if (currentLevel < NoOfLevels)
        {
            CodeGenObject.GetComponent<GenrateAQuestion>().GenrateImageAndCode(4);
            DisplayQuestionObject.GetComponent<DisplayQuestion>().displayfunction();
            
        }
        currentLevel++;
    }
    IEnumerator correctOrINcorrectAnim(int currentQuestionAnswer)
    {
        if (currentQuestionAnswer == 4)
        {
            Debug.Log("Correct Answer");
            Corect.SetActive(true);
            yield return new WaitForSeconds(1.5f);
        }
        else if (currentQuestionAnswer >= 1)
        {
            Debug.Log("Partial correct answer");
            partialCorrect.SetActive(true);
            yield return new WaitForSeconds(1.5f);
        }
        else
        {
            Debug.Log("Incorrect Answer");
            incorrect.SetActive(true);
            yield return new WaitForSeconds(1.5f);           
        }

        Corect.SetActive(false);
        partialCorrect.SetActive(false);
        incorrect.SetActive(false);
        yield return null;
    }
    public void playAnimation(int currentQuestionAnswer)
    {
        StartCoroutine(correctOrINcorrectAnim(currentQuestionAnswer));

    }
    public void NextQuestion()
    {
        if (currentLevel < NoOfLevels)
            StartCoroutine(StartGame());
        else
            GetResults.SetActive(true);
            
    }
}
