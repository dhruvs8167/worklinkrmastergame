﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timer : MonoBehaviour
{
    public float timeCount = 10f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(timerStart());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator timerStart()
    {
        while(timeCount >0)
        {
            yield return null;
            timeCount = timeCount - Time.deltaTime;
            
        }
        
    }
}
