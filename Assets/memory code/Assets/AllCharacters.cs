﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AllCharacters : MonoBehaviour
{   public CodeCharacters[] Obj= new CodeCharacters[36];

    
    // Start is called before the first frame update
    void Start()
    {
        InitializeCodes();
    }



    // Update is called once per frame



    void InitializeCodes()
    {
        for (int i = 0; i < 36; i++)
        {
            if (i < 10)
            {
                Obj[i].CharacterCode = (char)i;
                Obj[i].CharacterCodeString = i.ToString();
            }

            else
            {
                Obj[i].CharacterCode = (char)(65 + i - 10);
                Obj[i].CharacterCodeString = Obj[i].CharacterCode.ToString();
            }
            //print(i);
            Obj[i].a = i;
            Obj[i].avaliability = true;
        }
    }

    public void ResteTheAssigned()
    {
        for (int i = 0; i <36; i++)
        {
            Obj[i].avaliability = true;
        }
    }

}
