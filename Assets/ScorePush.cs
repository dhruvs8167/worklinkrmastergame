﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorePush : MonoBehaviour
{
    public float[] ScoreOfAllGames = new float[5];
    //0 is cloud
    //1 is memory
    //2 is bubble
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame

    public void setScore(int gameIndex, float GameScore)
    {
        ScoreOfAllGames[gameIndex] = GameScore;
    }
}
