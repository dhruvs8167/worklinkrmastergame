﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class loadScene : MonoBehaviour
{   // put any random image and text it noot needed
    public Text percentageText;
    public Image loadingImage;
    public GameObject LoadingBarsParent;
    // Start is called before the first frame update
    void Start()
    {
        LoadingBarsParent.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public IEnumerator LoadScene(int sceneIndex)
    {

        yield return null;
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        LoadingBarsParent.SetActive(true);
        while (operation.isDone == false)
        {
            //float percentage;
            //whilecount++;
            //percentage = operation.progress / 0.9f;
            float progress = Mathf.Clamp01(operation.progress / .9f);
            percentageText.text = "Loading "+(100 * progress).ToString() + '%';
            //percent1.text = whilecount.ToString();
            //Debug.Log (progress);
            loadingImage.fillAmount = progress;
            yield return null;

        }
    }
    /*
    public IEnumerator LoadScene(string sceneIndex)
    {

        yield return null;
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        LoadingBarsParent.SetActive(true);
        while (operation.isDone == false)
        {
            //float percentage;
            //whilecount++;
            //percentage = operation.progress / 0.9f;
            float progress = Mathf.Clamp01(operation.progress / .9f);
            percentageText.text = "Loading " + (100 * progress).ToString() + '%';
            //percent1.text = whilecount.ToString();
            //Debug.Log (progress);
            loadingImage.fillAmount = progress;
            yield return null;

        }
    }*/
    public void LoadSceneByNameCall(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
        //StartCoroutine(LoadSceneByName(sceneName));
    }
    public void LoadSceneCall(int a)
    {
        StartCoroutine(LoadScene(a));
    }
  /*  public void LoadSceneCall(string a)
    {   
        StartCoroutine(LoadScene(a));
    }*/

    public void exitGame()
    {
        Application.Quit();
    }
}
